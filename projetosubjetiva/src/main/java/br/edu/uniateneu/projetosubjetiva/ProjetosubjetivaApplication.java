package br.edu.uniateneu.projetosubjetiva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetosubjetivaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetosubjetivaApplication.class, args);
	}

}
